<?php
//フォームデータが空の場合の場合
if (empty($_POST)) {
   echo "処理終了";
   exit;
}
//セッションの開始
session_start();
?>

<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title></title>
      <meta charset="utf-8">
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="">
      <!--&#91;if lt IE 9&#93;>
      <script src="//cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
      <!&#91;endif&#93;-->
      <link rel="shortcut icon" href="">
      <link rel="stylesheet" href="" type="text/css" />
      <style>
      .sab_mlbox{
         padding: 20px;
      }
      .sab_mlbox table{
         background: #ccc;
      }
      .sab_mlbox table td{
         background: #FFF;
         line-height: 1.5;
         padding: 10px;
      }
      .sab_mlbox table td input.text{
         background-color: #FFFFE1;
         border:1px solid #ccc;
         padding:8px 10px;
         color: #444;
         font-size: 12px;
         width: 70%;
      }
      .sab_mlbox table td textarea{
         background-color: #FFFFE1;
         border:1px solid #ccc;
         padding:8px 10px;
         color: #444;
         font-size: 12px;
         line-height: 1.5;
         width: 80%;
      }
      .sab_mlbox table td input.submit{
         padding: 16px 50px;
         color: #444;
      }
      .sab_mlbox table td.tdl{
         background: #F6F6F6;
      }
      .sab_mlbox table td.tdl img{
         padding-top: 2px;
         padding-left: 6px;
      }
      span.red {
          color: #ff0000;
      }
      </style>
   </head>
   <body>
      <?php
      //入力値の収得•加工
         $name = htmlspecialchars($_POST['name'], ENT_QUOTES, "UTF-8");
         $name2 = htmlspecialchars($_POST['name2'], ENT_QUOTES, "UTF-8");
         $cp = htmlspecialchars($_POST['cp'], ENT_QUOTES, "UTF-8");
         $chimei = htmlspecialchars($_POST['chimei'], ENT_QUOTES, "UTF-8");
         $banchi = htmlspecialchars($_POST['banchi'], ENT_QUOTES, "UTF-8");
         $mail = htmlspecialchars($_POST['mail'], ENT_QUOTES, "UTF-8");
         $tel = htmlspecialchars($_POST['tel'], ENT_QUOTES, "UTF-8");
         $naiyou = htmlspecialchars($_POST['naiyou'], ENT_QUOTES, "UTF-8");

         $_SESSION["name"] = $name;
         $_SESSION["name2"] = $name2;
         $_SESSION["cp"] = $cp;
         $_SESSION["chimei"] = $chimei;
         $_SESSION["banchi"] = $banchi;
         $_SESSION["mail"] = $mail;
         $_SESSION["tel"] = $tel;
         $_SESSION["naiyou"] = $naiyou;
      ?>
      <div class="sab_mlbox">
         <form action="send.php" method="post">
            <table width="100%" cellspacing="1" cellpadding="0" style="margin-bottom:20px;">
               <tr>
                  <td width="160" class="tdl"><span class="wt">お名前</span>※</td>
                  <td width="520"><input class="text" type="hidden" name="name" value="<?php echo $name; ?>"><?php echo $name; ?>
                     <?
                        if($name == ""){
                        print "<span class=\"red\">お名前が未入力です。</span>\n";
                        $err +=1;
                        }
                     ?>
                  </td>
               </tr>
               <tr>
                  <td width="160" class="tdl"><span class="wt">ふりがな</span></td>
                  <td width="520"><input class="text" type="hidden" name="name2" value="<?php echo $name2; ?>"><?php echo $name2; ?>
                  </td>
               </tr>
               <tr>
                  <td width="160" class="tdl"><span class="wt">会社名</span></td>
                  <td width="520"><input class="text" type="hidden" name="cp" value="<?php echo $cp; ?>"><?php echo $cp; ?></td>
               </tr>
               <tr>
                  <td width="160" class="tdl"><span class="wt">ご住所</span>※</td>
                  <td width="520">都道府県・市区町村　<input class="text" type="hidden" name="chimei" value="<?php echo $chimei; ?>"><?php echo $chimei; ?>
                     <?
                        if($chimei == ""){
                        print "<span class=\"red\">都道府県・市区町村名が未入力です。</span>\n";
                        $err +=1;
                        }
                     ?>
                  </td>
               </tr>
               <tr>
                  <td width="160" class="tdl"><span class="wt">ご住所</span></td>
                  <td width="520">
                     番地・ビル名　<input class="text" type="hidden" name="banchi" value="<?php echo $banchi; ?>"><?php echo $banchi; ?>
                  </td>
               </tr>
               <tr>
                  <td width="160" class="tdl"><span class="wt">メールアドレス</span>※</td>
                  <td width="520"><input class="text" type="hidden" name="mail" value="<?php echo $mail; ?>"><?php echo $mail; ?>
                     <?
                        if($mail == ""){
                        print "<span class=\"red\">メールアドレスが未入力です。</span>\n";
                        $err +=1;
                        }
                     ?>
                  </td>
               </tr>
               <tr>
                  <td width="160" class="tdl"><span class="wt">電話番号</span></td>
                  <td width="520"><input class="text" type="hidden" name="tel" value="<?php echo $tel; ?>"><?php echo $tel; ?>
                  </td>
               </tr>
            </table>
            <table width="100%" cellspacing="1" cellpadding="0">
               <tr>
                  <td width="160" class="tdl"><span class="wt">ご依頼項目</span></td>
                  <td width="520" style="line-height:2;">
                     <?
                        $cbs = "";
                        for($i=0; $i<count($_POST['cb']); $i++){
                          $cbs .= $_POST['cb'][$i]."\n";
                          echo $_POST['cb'][$i]."<br />\n";
                        }
                     ?>
                     <input class="text" type="hidden" name="cbs_ch" value="<?=$cbs;?>">
                  </td>
               </tr>
               <tr>
                  <td width="160" class="tdl"><span class="wt">内容</span></td>
                  <td width="520"><input type="hidden" name="naiyou" value="<?php echo $naiyou; ?>"><?php echo nl2br($naiyou); ?></td>
               </tr>
            </table>
            <table width="100%" cellspacing="0" cellpadding="0" style="background-color:#FFF;">
               <tr>
                  <td width="" align="center" valign="top" colspan="" style="padding:20px;">
                     <?
                        if($err == 0){
                          print "<input type=\"button\" value=\"内容の修正\" onclick=\"history.back()\" class=\"submit\"> <input type=\"submit\" value=\"この内容で送信する\" class=\"submit\">";
                        }else{
                          print "<input type=\"button\" value=\"内容の修正\" onclick=\"history.back()\" class=\"submit\">";
                        }
                     ?>
                  </td>
               </tr>
            </table>
         </form>
      </div>
   </body>
</html>